using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace lab7
{
	/// <summary>
	/// Server.
	/// </summary>
	public class server
	{
		/// <summary>
		/// The entry point of the program, where the program control starts and ends.
		/// </summary>
		public static void Main()
		{
			DoSocket("localhost", 5051);
		}
		/// <summary>
		/// Dos the socket.
		/// </summary>
		/// <param name='server'>
		/// Server.
		/// </param>
		/// <param name='conPort'>
		/// Con port.
		/// </param>
		public static void DoSocket(string server, int conPort)
		{
			try
			{
				//Сокет
				Socket s = null;
				//Представляет сетевую конечную точку в виде IP-адреса и номера порта
				IPEndPoint hostEndPoint;
				IPAddress hostAddress = null;
				//Запрашиваем у DNS-сервера IP-адрес, связанный с именем узла или его IP-адресом
				IPHostEntry hostInfo = Dns.GetHostEntry(server);
				//Возвращает список IP-адресов, связанных с узлом
				IPAddress[] IPaddresses = hostInfo.AddressList;
				//Вычисляем сокет, получаем IP-адрес узла и сетевую конечную точку
				for (int i = 0; i < IPaddresses.Length; i++)
				{
					hostAddress = IPaddresses[i];
					hostEndPoint = new IPEndPoint(hostAddress, conPort);
					//Создаем сокет для передачи данных через TCP соединение
					s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
					//Связываем объект Socket с локальной конечной точкой
					s.Bind(hostEndPoint);
					//Устанавливаем объект Socket в состояние прослушивания
					s.Listen(int.MaxValue);
					Console.WriteLine("Server started.");
					Console.WriteLine("Listening on " + hostEndPoint.Address.ToString());
					while (true)
					{
						try
						{
							//Создаем новый объект Socket для заново созданного подключения
							Socket clientSocket = s.Accept();
							Console.WriteLine("Accepted connection from: {0}",
							clientSocket.RemoteEndPoint.ToString());
							server handler = new server();
							handler.SetSocket = clientSocket;
							handler.Start();
						}
						catch (Exception ex)
						{
							Console.WriteLine("client connection failed: {0}", ex.Message);
						}
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("SocketException caught!!!");
				Console.WriteLine("Source : " + e.Source);
				Console.WriteLine("Message : " + e.Message);
			}
		
		}
		/// <summary>
		/// The socket.
		/// </summary>
		private Socket socket;
		/// <summary>
		/// The thread.
		/// </summary>
		private Thread thread;
		/// <summary>
		/// The models.
		/// </summary>
		private Dictionary < int, model > models = new Dictionary<int, model>();
		/// <summary>
		/// The max identifier.
		/// </summary>
		private int maxId = 0;
		/// <summary>
		/// Initializes a new instance of the <see cref="lab7.server"/> class.
		/// </summary>
		public server ()
		{
		}
		/// <summary>
		/// Start this instance.
		/// </summary>
		public void Start()
		{
			thread = new Thread(new ThreadStart(ClientListener));
			thread.Priority = ThreadPriority.Normal;
			thread.Start();
		}
		/// <summary>
		/// Sets the set socket.
		/// </summary>
		/// <value>
		/// The set socket.
		/// </value>
		public Socket SetSocket
		{
			set
			{
				socket = value;
			}
		}
		/// <summary>
		/// Clients the listener.
		/// </summary>
		public void ClientListener()
		{
			//networkStream = new NetworkStream(socket);
			//reader = new StreamReader(networkStream);
			//writer = new BinaryWriter(networkStream);
			StreamReader reader = new StreamReader(new NetworkStream(socket));
			StreamWriter writer = new StreamWriter(new NetworkStream(socket));
			bool start = true;
			while (start)
			{
				try
				{
					//int x = networkStream.ReadByte();
					//while (!networkStream.DataAvailable);
					//networkStream = new NetworkStream(socket);
					string clientMessage = reader.ReadLine();
					if (clientMessage == null) continue;
					//clientMessage = clientMessage.Substring(1);
					Console.WriteLine(clientMessage);
					//string bga1 = reader.ReadLine();
					//string bga2 = reader.ReadLine();
					//string bga3 = reader.ReadLine();
					int clientId = Convert.ToInt32(clientMessage.Split(' ')[0]);
					string message = clientMessage.Remove(0, clientMessage.Split(' ')[0].Length + 1);
					// client asks for id
					
					if (clientId == 0)
					{
						++maxId;
						clientId = maxId;
						models[clientId] = new model();
						writer.WriteLine(clientId.ToString());
					}
					else if (message == "exit")
					{
						start = false;
						writer.WriteLine("Terminated");
					}
					else
					{
						writer.WriteLine(models[clientId].gotSomething(message));
					}
					writer.Flush();
					writer.BaseStream.Flush();
					//writer.Close();
					//writer = null;
					//reader = null;
					//networkStream.Flush();
				}
				catch (Exception e)
				{
					Console.WriteLine("Error: {0}", e.Message);
				}
			}
		}
	}
}

