using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;
using Mono.Unix;

namespace lab7
{
	/// <summary>
	/// Model.
	/// </summary>
	public class model
	{
		/// <summary>
		/// The dbcon.
		/// </summary>
		private MySqlConnection dbcon = null;
		/// <summary>
		/// The parameters.
		/// </summary>
		private Dictionary < string, object > parameters = new Dictionary<string, object>();
		/// <summary>
		/// The command.
		/// </summary>
		private string command;
		/// <summary>
		/// The is query.
		/// </summary>
		private bool isQuery;
		/// <summary>
		/// The ready.
		/// </summary>
		private bool ready;
		/// <summary>
		/// The just query.
		/// </summary>
		private bool justQuery;
		/// <summary>
		/// The result.
		/// </summary>
		private string[][] result;
		/// <summary>
		/// The state.
		/// </summary>
		private string state;
		/// <summary>
		/// The keystate.
		/// </summary>
		private string keystate;
		/// <summary>
		/// The column_counter.
		/// </summary>
		private int column_counter;
		/// <summary>
		/// The row_counter.
		/// </summary>
		private int row_counter;
		/// <summary>
		/// Gets the connection.
		/// </summary>
		/// <returns>
		/// The connection.
		/// </returns>
		private MySqlConnection getConnection()
		{
			if (dbcon == null)
			{
				string constr = 
					"Server=localhost;" +
					"Database=market;" +
					"User ID=root;" +
					"Password=123Vjq123;" +
					"Pooling=false";
				dbcon = new MySqlConnection(constr);
				dbcon.Open();
			}
			return dbcon;
		}
		/// <summary>
		/// Gets the command.
		/// </summary>
		/// <returns>
		/// The command.
		/// </returns>
		private MySqlCommand getCommand()
		{
			return getConnection().CreateCommand();
		}
		/// <summary>
		/// Gets the data from stored procedure.
		/// </summary>
		/// <returns>
		/// The data from stored procedure.
		/// </returns>
		/// <param name='storedProcedureName'>
		/// Stored procedure name.
		/// </param>
		/// <param name='parameters'>
		/// Parameters.
		/// </param>
		private MySqlDataReader getDataFromStoredProcedure(
			string storedProcedureName, 
			Dictionary < string, object > parameters)
		{
			MySqlCommand cmd = getCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = storedProcedureName;
			if (parameters != null)
				foreach (var x in parameters)
			{
				cmd.Parameters.Add (x.Key, x.Value);
			}
			return cmd.ExecuteReader();
		}
		/// <summary>
		/// Runs the query.
		/// </summary>
		/// <param name='query'>
		/// Query.
		/// </param>
		/// <param name='parameters'>
		/// Parameters.
		/// </param>
		private void runQuery(
			string query, 
			Dictionary < string, object > parameters)
		{
			MySqlCommand cmd = getCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = query;
			if (parameters != null)
				foreach (var x in parameters)
			{
				cmd.Parameters.Add (x.Key, x.Value);
			}
			cmd.ExecuteNonQuery();
		}
		/// <summary>
		/// Gets the data from query.
		/// </summary>
		/// <returns>
		/// The data from query.
		/// </returns>
		/// <param name='query'>
		/// Query.
		/// </param>
		/// <param name='parameters'>
		/// Parameters.
		/// </param>
		private MySqlDataReader getDataFromQuery(
			string query, 
			Dictionary < string, object > parameters)
		{
			MySqlCommand cmd = getCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = query;
			if (parameters != null)
				foreach (var x in parameters)
			{
				cmd.Parameters.Add (x.Key, x.Value);
			}
			return cmd.ExecuteReader();
		}
		/// <summary>
		/// Parses the data.
		/// </summary>
		/// <returns>
		/// The data.
		/// </returns>
		/// <param name='reader'>
		/// Reader.
		/// </param>
		private string[][] parseData(MySqlDataReader reader)
		{
			List < string[] > res = new List<string[]>();
			bool firstPass = true;
			int n = 0, len = 0;
			string[] header;
			string[] row = null;
			while (reader.Read())
			{
				if (firstPass)
				{
					len = reader.FieldCount;
					header = new string[len];
					for (int i = 0; i < len; ++i)
					{
						header[i] = reader.GetName(i);
					}
					res.Add(header);
					firstPass = false;
				}
				row = new string[len];
				for (int i = 0; i < len; ++i)
				{
					row[i] = reader.GetString(i);
				}
				res.Add(row);
			}
			reader.Close();
			if (firstPass)
			{
				return null;
			}
			return res.ToArray();
		}
		/// <summary>
		/// Starts the operation.
		/// </summary>
		private void startOperation()
		{
			isQuery = false;
			parameters.Clear();
			command = "";
			ready = false;
			justQuery = false;
			result = null;
			state = "wait_isquery";
		}
		/// <summary>
		/// Sets the is query.
		/// </summary>
		/// <param name='param'>
		/// Parameter.
		/// </param>
		private void setIsQuery(string param)
		{
			if (param == "false")
			{
				isQuery = false;
				state = "wait_justquery";
			}
			else
			{
				isQuery = true;
				state = "wait_justquery";
			}
		}
		/// <summary>
		/// Sets the just query.
		/// </summary>
		/// <param name='param'>
		/// Parameter.
		/// </param>
		private void setJustQuery(string param)
		{
			if (param == "false")
				justQuery = false;
			else justQuery = true;
			state = "wait_query";
		}
		/// <summary>
		/// Sets the query.
		/// </summary>
		/// <param name='param'>
		/// Parameter.
		/// </param>
		private void setQuery(string param)
		{
			command = param;
			state = "no_state";
		}
		/// <summary>
		/// Does operation.
		/// </summary>
		private void doOperation()
		{
			if (isQuery && justQuery)
			{
				result = null;
				runQuery(command, parameters);
				state = "finished";
				return;
			}
			if (isQuery)
			{
				result = parseData(getDataFromQuery(command, parameters));
				state = "got_header_count";
				return;
			}
			result = parseData(getDataFromStoredProcedure(command, parameters));
			state = "got_header_count";
		}
		/// <summary>
		/// Sets the state.
		/// </summary>
		/// <param name='state'>
		/// State.
		/// </param>
		private void setState(string state)
		{
			this.state = state;
		}
		/// <summary>
		/// Sets the key.
		/// </summary>
		/// <param name='param'>
		/// Parameter.
		/// </param>
		private void setKey(string param)
		{
			keystate = param;
			state = "wait_value";
		}
		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name='param'>
		/// Parameter.
		/// </param>
		private void setValue(string param)
		{
			parameters.Add (keystate, param);
			state = "no_state";
		}
		/// <summary>
		/// Gots something.
		/// </summary>
		/// <returns>
		/// The something.
		/// </returns>
		/// <param name='what'>
		/// What.
		/// </param>
		public string gotSomething(string what)
		{
			Console.WriteLine("state: {1}; got: {0}", what, state);
			string res = "";
			if (state == "no_state")
			{
				state = what;
			}
			else if (state == "wait_key")
			{
				setKey(what);
			}
			else if (state == "wait_value")
			{
				setValue(what);
			}
			else if (state == "wait_do" || state == "finished")
			{
				if (what == "start")
				{
					startOperation();
				}
				else if (what == "operation")
				{
					doOperation();
				}
				else if (state == "finished")
				{
					res = "finished";
				}
			}
			else if (state == "wait_isquery")
			{
				setIsQuery(what);
			}
			else if (state == "wait_justquery")
			{
				setJustQuery(what);
			}
			else if (state == "wait_query")
			{
				setQuery(what);
			}
			else if (state == "finished")
			{
				res = "Error:finished";
			}
			else if (state == "got_header_count")
			{
				if (result == null)
				{
					res = "0";
					state = "got_row_count";
				}
				else
				{
					string[] header = result[0];
					res = header.Length.ToString();
					state = "got_header";
					column_counter = 0;
					row_counter = 0;
				}
			}
			else if (state == "got_header")
			{
				string[] header = result[0];
				res = header[column_counter];
				column_counter++;
				if (column_counter >= header.Length)
				{
					column_counter = 0;
					row_counter = 1;
					state = "got_row_count";
				}
			}
			else if (state == "got_row_count")
			{
				if (result == null)
				{
					res = "0";
					state = "finished";
				}
				else
				{
					res = (result.Length - 1).ToString();
					state = "got_row";
				}
			}
			else if (state == "got_row")
			{
				string[] row = result[row_counter];
				res = row[column_counter];
				column_counter++;
				if (column_counter >= row.Length)
				{
					column_counter = 0;
					row_counter++;
					if (row_counter >= result.Length)
					{
						state = "finished";
					}
				}
			}
			Console.WriteLine("new state: {1}; sent back: {0}", res, state);
			return res;
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="lab7.model"/> class.
		/// </summary>
		public model ()
		{
			ready = false;
			state = "wait_do";
		}
	}
}

