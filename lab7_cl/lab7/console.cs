using System;
using System.Net;
using System.Net.Sockets;
using System.Data;
using System.IO;

namespace lab7
{
	/// <summary>
	/// Console.
	/// </summary>
	/// <exception cref='ArgumentException'>
	/// Is thrown when an argument passed to a method is invalid.
	/// </exception>
	public class console
	{
		/// <summary>
		/// The host end point.
		/// </summary>
		private IPEndPoint hostEndPoint;
		/// <summary>
		/// The client.
		/// </summary>
		private Socket client;
		/// <summary>
		/// The host info.
		/// </summary>
		IPHostEntry hostInfo;
		/// <summary>
		/// The con port.
		/// </summary>
		int conPort;
		/// <summary>
		/// Initializes a new instance of the <see cref="lab7.console"/> class.
		/// </summary>
		/// <param name='server'>
		/// Server.
		/// </param>
		/// <param name='conPort'>
		/// Con port.
		/// </param>
		public console (string server, int conPort)
		{
			this.conPort = conPort;
			client = null;
			hostInfo = Dns.GetHostEntry(server);
		}
		/// <summary>
		/// News the connection.
		/// </summary>
		/// <exception cref='ArgumentException'>
		/// Is thrown when an argument passed to a method is invalid.
		/// </exception>
		public void NewConnection()
		{
			IPAddress hostAddress = null;
			try
			{
				//Возвращает список IP-адресов, связанных с узлом
				IPAddress[] IPaddresses = hostInfo.AddressList;
				//Вычисляем сокет, получаем IP-адрес узла и сетевую конечную точку
				for (int i = 0; i < IPaddresses.Length; i++)
				{
					hostAddress = IPaddresses[i];
					hostEndPoint = new IPEndPoint(hostAddress, conPort);
					//Создаем сокет для передачи данных через TCP соединение
					client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
					//Устанавливаем соединение
					client.Connect(hostEndPoint);
				}
			}
			catch (Exception)
			{
				throw new ArgumentException("Socket connection failed");
			}
		}
		/// <summary>
		/// Closes the connection.
		/// </summary>
		/// <exception cref='ArgumentException'>
		/// Is thrown when an argument passed to a method is invalid.
		/// </exception>
		public void CloseConnection()
		{
			//Проверка на наличие соединения
			if (!client.Connected)
				return;
			
			StreamWriter writer = null;
			try
			{
				NetworkStream networkStream = new NetworkStream(client);
				//Посылаем команду серверу
				writer = new StreamWriter(networkStream);
				string clientMessage = "exit";
				writer.WriteLine(clientMessage);
				writer.Flush();
			}
			catch (Exception)
			{
				throw new ArgumentException();
			}
		}
		/// <summary>
		/// Sends the command.
		/// </summary>
		/// <returns>
		/// The command.
		/// </returns>
		/// <param name='command'>
		/// Command.
		/// </param>
		public string SendCommand(string command)
		{
			//NetworkStream networkStream = ;
			StreamWriter writer = new StreamWriter(new NetworkStream(client));
			StreamReader reader = new StreamReader(new NetworkStream(client));
			try
			{
				string clientMessage = command;
				Console.WriteLine(clientMessage);
				writer.WriteLine(clientMessage);
				writer.Flush();
				writer.BaseStream.Flush();
				//writer.Close();
				//writer = null;
				//networkStream.Flush();
				Console.WriteLine("i have flushed");
				//while (!networkStream.DataAvailable);
				string response = reader.ReadLine();
				Console.WriteLine(response);
				return response;
			}
			catch (Exception e)
			{
				Console.WriteLine("Query failed: {0}", e.Message);
			}
			return "error";
		}
	}
}

