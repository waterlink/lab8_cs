/*
 * Author: Fedorov Alexey
 * KKT
 * DonNU
 * 2012
 * Lab8 Var 2
 * Task:
 * 		- create database with three tables
 * 		- create stored procedures for work with data
 * 		- create dialog application to work with them (show tables, show special queries
 * edit tables, delete/add rows, create special query-builder (filters))
 * 		- thematics is market (items, storages and presences of items at storages)
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace lab7
{
	/// <summary>
	/// Mainform - main dialog form of application.
	/// </summary>
	public class mainform : Form
	{
		/// <summary>
		/// The entry point of the program, where the program control starts and ends.
		/// </summary>
		public static void Main()
		{
			Application.Run(new mainform());
		}
		
		/// <summary>
		/// The database connection object.
		/// </summary>
		private MySqlConnection dbcon;
		/// <summary>
		/// The chosen main table.
		/// </summary>
		private DataGridView ChosenMainTable = new DataGridView();
		/// <summary>
		/// The main table choicer.
		/// </summary>
		private ComboBox MainTableChoicer = new ComboBox();
		/// <summary>
		/// The main table choicer label.
		/// </summary>
		private Label MainTableChoicerLabel = new Label();
		/// <summary>
		/// The secondary table.
		/// </summary>
		private DataGridView SecondaryTable = new DataGridView();
		/// <summary>
		/// The primary table label.
		/// </summary>
		private Label PrimaryTableLabel = new Label();
		/// <summary>
		/// The max identifier.
		/// </summary>
		private int maxId = 0;
		/// <summary>
		/// The current identifier.
		/// </summary>
		private int currentId = 0;
		/// <summary>
		/// The deleting enabled.
		/// </summary>
		private bool deletingEnabled;
		/// <summary>
		/// The con.
		/// </summary>
		private console con = new console("localhost", 5051);
		private string uid = "0 ";
		
		/// <summary>
		/// Initializes the primary table label.
		/// </summary>
		private void InitializePrimaryTableLabel()
		{
			PrimaryTableLabel.Text = "";
			PrimaryTableLabel.Location = new Point(0, 60);
			PrimaryTableLabel.Size = new Size(200, 40);
		}
		/// <summary>
		/// Raises the primary table cell edit event.
		/// </summary>
		/// <param name='sender'>
		/// Sender.
		/// </param>
		/// <param name='e'>
		/// E.
		/// </param>
		public void OnPrimaryTableCellEdit(object sender, DataGridViewCellEventArgs e)
		{
			int ci = e.ColumnIndex;
			int ri = e.RowIndex;
			string field = ChosenMainTable.Columns[ci].Name;
			if (GetMainTableChoice() == "query" || field == "id")
			{
				ChangeChosenTable();
				return;
			}
			int id = 0;
			try
			{
				id = Convert.ToInt32(ChosenMainTable.Rows[ri].Cells["id"].Value.ToString());
			}
			catch (Exception ex)
			{
				//ChosenMainTable.Rows.Add();
				maxId++;
				id = maxId;
				ChosenMainTable.Rows[ri].Cells["id"].Value = id;
			}
			string val = "";
			string query = "update " + GetTable() + " set " + field + " = @val where id = @id";
			if (ChosenMainTable.Rows[ri].Cells[ci].Value != null)
			{
				val = ChosenMainTable.Rows[ri].Cells[ci].Value.ToString();
			}
			else
			{
				query = "insert into " + GetTable() + "(" + field + ") values(\"\")";
			}
			Dictionary < string, object > param = new Dictionary<string, object>();
			param.Add("@id", id);
			param.Add("@val", val);
			RunQuery(query, param, true);
		}
		/// <summary>
		/// Raises the primary table cell click event.
		/// </summary>
		/// <param name='sender'>
		/// Sender.
		/// </param>
		/// <param name='e'>
		/// E.
		/// </param>
		/// 
		public void OnPrimaryTableCellClick(object sender, DataGridViewCellEventArgs e)
		{
			int ci = e.ColumnIndex;
			int ri = e.RowIndex;
			string field = ChosenMainTable.Columns[ci].Name;
			// so, here we have items
			Dictionary < string, object > parameters = new Dictionary <string, object>();
			string command = "getPresencesFor";
			int id = 0;
			if (GetMainTableChoice() == "Items")
			{
				try
				{
					id = Convert.ToInt32(ChosenMainTable.Rows[ri].Cells["id"].Value.ToString());
				}
				catch (Exception ex)
				{
					return;
				}
				parameters.Add("theitemid", id);
			}
			else if (GetMainTableChoice() == "Storages")
			{
				try
				{
					id = Convert.ToInt32(ChosenMainTable.Rows[ri].Cells["id"].Value.ToString());
				}
				catch (Exception ex)
				{
					return;
				}
				parameters.Add("thestorageid", id);
				command = "getItemsAt";
			}
			else if (GetMainTableChoice() == "Presences" && field != "id")
			{
				try
				{
					id = Convert.ToInt32(ChosenMainTable.Rows[ri].Cells[field].Value.ToString());
				}
				catch (Exception ex)
				{
					return;
				}
				parameters.Add ("byid", id);
				command = string.Format("getPresenceDataBy{0}", field);
			}
			else return;
			if (id == null) return;
			//ExposeReaderToTable(
			//	GetDataFromStoredProcedure(command, parameters),
			//	SecondaryTable);
			GetDataFromStoredProcedure(command, parameters, SecondaryTable);
		}
		/// <summary>
		/// Raises the primary table row add event.
		/// </summary>
		/// <param name='sender'>
		/// Sender.
		/// </param>
		/// <param name='e'>
		/// E.
		/// </param>
		public void OnPrimaryTableRowAdd(object sender, DataGridViewRowEventArgs e)
		{
			MessageBox.Show("bga");
		}
		/// <summary>
		/// Raises the primary table row delete event.
		/// </summary>
		/// <param name='sender'>
		/// Sender.
		/// </param>
		/// <param name='e'>
		/// E.
		/// </param>
		public void OnPrimaryTableRowDelete(object sender, DataGridViewRowEventArgs e)
		{
			int id = 0;
			try
			{
				id = Convert.ToInt32(e.Row.Cells["id"].Value.ToString());
			}
			catch (Exception ex)
			{
				return;
			}
			Dictionary <string, object> parameters = new Dictionary<string, object>();
			parameters.Add ("@id", id);
			string query = "delete from " + GetTable() + " where id = @id";
			RunQuery(query, parameters, true);
		}
		/// <summary>
		/// Raises the primary table row delete2 event.
		/// </summary>
		/// <param name='sender'>
		/// Sender.
		/// </param>
		/// <param name='e'>
		/// E.
		/// </param>
		public void OnPrimaryTableRowDelete2(object sender, DataGridViewRowCancelEventArgs e)
		{
			int id = 0;
			try
			{
				id = Convert.ToInt32(e.Row.Cells["id"].Value.ToString());
			}
			catch (Exception ex)
			{
				return;
			}
			Dictionary <string, object> parameters = new Dictionary<string, object>();
			parameters.Add ("@id", id);
			string query = "delete from " + GetTable() + " where id = @id";
			RunQuery(query, parameters, true);
			currentId = 0;
		}
		/// <summary>
		/// Raises the primary table row delete3 event.
		/// </summary>
		/// <param name='sender'>
		/// Sender.
		/// </param>
		/// <param name='e'>
		/// E.
		/// </param>
		public void OnPrimaryTableRowDelete3(object sender, DataGridViewRowsRemovedEventArgs e)
		{
			int id = 0;
			try
			{
				//id = Convert.ToInt32(ChosenMainTable.Rows[e.RowIndex].Cells["id"].Value.ToString());
				id = currentId;
				if (id == 0) return;
			}
			catch (Exception ex)
			{
				return;
			}
			Dictionary <string, object> parameters = new Dictionary<string, object>();
			parameters.Add ("@id", id);
			string query = "delete from " + GetTable() + " where id = @id";
			if (deletingEnabled)
				RunQuery(query, parameters, true);
		}
		/// <summary>
		/// Raises the primary table selection changed event.
		/// </summary>
		/// <param name='sender'>
		/// Sender.
		/// </param>
		/// <param name='e'>
		/// E.
		/// </param>
		public void OnPrimaryTableSelectionChanged(object sender, EventArgs e)
		{
			try
			{
				currentId = Convert.ToInt32(ChosenMainTable.CurrentRow.Cells["id"].Value.ToString());
			}
			catch (Exception ex)
			{
				currentId = 0;
			}
		}
		/// <summary>
		/// Initializes the chosen main table.
		/// </summary>
		private void InitializeChosenMainTable()
		{
			ChosenMainTable.Location = new Point(0, 100);
			ChosenMainTable.Size = new Size(300, 200);
			ChosenMainTable.CellClick += OnPrimaryTableCellClick;
			ChosenMainTable.CellEndEdit += OnPrimaryTableCellEdit;
			ChosenMainTable.NewRowNeeded += OnPrimaryTableRowAdd;
			ChosenMainTable.UserDeletedRow += OnPrimaryTableRowDelete;
			ChosenMainTable.UserDeletingRow += OnPrimaryTableRowDelete2;
			ChosenMainTable.RowsRemoved += OnPrimaryTableRowDelete3;
			ChosenMainTable.SelectionChanged += OnPrimaryTableSelectionChanged;
		}
		/// <summary>
		/// Initializes the secondary table.
		/// </summary>
		private void InitializeSecondaryTable()
		{
			SecondaryTable.Location = new Point(0, 300);
			SecondaryTable.Size = new Size(300, 200);
			SecondaryTable.AllowUserToAddRows = false;
			SecondaryTable.AllowUserToDeleteRows = false;
		}
		/// <summary>
		/// Initializes the main table choicer label.
		/// </summary>
		private void InitializeMainTableChoicerLabel()
		{
			MainTableChoicerLabel.Text = "Choose main table to view";
			MainTableChoicerLabel.Location = new Point(10, 26);
			MainTableChoicerLabel.Size = new Size(100, 30);
		}
		/// <summary>
		/// Initializes the main table choicer.
		/// </summary>
		private void InitializeMainTableChoicer()
		{
			MainTableChoicer.Items.Add ("Items");
			MainTableChoicer.Items.Add ("Storages");
			MainTableChoicer.Items.Add ("Presences");
			MainTableChoicer.Items.Add ("query");
			MainTableChoicer.Location = new Point(100, 30);
			MainTableChoicer.Size = new Size(60, 30);
			MainTableChoicer.TextChanged += (sender, e) => ChangeChosenTable();
		}
		/// <summary>
		/// Initializes the main form.
		/// </summary>
		private void InitializeMainForm()
		{
			ClientSize = new Size(300, 500);
			Text = "Market";
			FormBorderStyle = FormBorderStyle.FixedSingle;
			AddControlsToForm();
		}
		/// <summary>
		/// Adds the controls to form.
		/// </summary>
		private void AddControlsToForm()
		{
			Controls.Add(MainTableChoicer);
			Controls.Add(ChosenMainTable);
			Controls.Add(MainTableChoicerLabel);
			Controls.Add(SecondaryTable);
			Controls.Add(PrimaryTableLabel);
		}
		/// <summary>
		/// Initializes the component.
		/// </summary>
		private void InitializeComponent()
		{
			InitializeMainTableChoicer();
			InitializeMainTableChoicerLabel();
			InitializeChosenMainTable();
			InitializeSecondaryTable();
			InitializePrimaryTableLabel();
			InitializeMainForm();
		}
		/// <summary>
		/// Gets the main table choice.
		/// </summary>
		/// <returns>
		/// The main table choice.
		/// </returns>
		private string GetMainTableChoice()
		{
			if (MainTableChoicer.SelectedItem == null) return "null";
			return MainTableChoicer.SelectedItem.ToString();
		}
		/// <summary>
		/// Exposes the reader to table.
		/// </summary>
		/// <param name='dr'>
		/// Dr.
		/// </param>
		/// <param name='gv'>
		/// Gv.
		/// </param>
		private void ExposeReaderToTable(MySqlDataReader dr, DataGridView gv)
		{
			deletingEnabled = false;
			bool firstPass = true;
			//for (int i = 0; i < gv.Rows.Count; ++i)
			//	gv.Rows.RemoveAt(i);
			//gv.ResetCursor();
			//gv.Rows.Clear();
			//new DataGridViewRowCollection(gv);
			// bugfix here
			if (gv.Rows.Count > 0 && gv.Rows[0].Cells.Count > 0)
				gv.CurrentCell = gv.Rows[0].Cells[0];
			gv.Columns.Clear();
			maxId = 0;
			int n = 0;
			while (dr.Read ())
			{
				if (gv == ChosenMainTable)
				{
					int id = dr.GetInt32("id");
					if (id > maxId) maxId = id;
				}
				if (firstPass)
				{
					for (int i = 0; i < dr.FieldCount; ++i)
					{
						gv.Columns.Add(dr.GetName(i), dr.GetName(i));
						//MessageBox.Show(dr.GetName(i));
						Console.WriteLine(dr.GetName (i));
					}
					firstPass = false;
				}
				//string[] row = new string[dr.FieldCount];
				try
				{
					//gv.Rows.Insert(0, 1);
					n = gv.Rows.Add ();
				}
				catch (Exception e)
				{
					//MessageBox.Show(e.Message);
					Console.WriteLine(e.Message);
				}
				for (int i = 0; i < dr.FieldCount; ++i)
					//row[i] = dr.GetString(i);
					gv.Rows[n].Cells[i].Value = dr.GetString(i);
				//ChosenMainTable.Rows.Add(row);
			}
			//ChosenMainTable.DataSource = dr;
			dr.Close();
			deletingEnabled = true;
		}
		/// <summary>
		/// Runs the query.
		/// </summary>
		/// <param name='query'>
		/// Query.
		/// </param>
		/// <param name='parameters'>
		/// Parameters.
		/// </param>
		private void RunQuery(string query, Dictionary < string, object > parameters, bool scalar = false)
		{
			/*MySqlCommand cmd = dbcon.CreateCommand();
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = query;
			foreach (var x in parameters)
			{
				cmd.Parameters.Add(x.Key, x.Value);
			}
			MySqlDataReader dr;
			if (scalar)
			{
				cmd.ExecuteNonQuery();
				GetMainTableChoice();
			}
			else 
			{
				dr = cmd.ExecuteReader();
				ExposeReaderToTable(dr, ChosenMainTable);
			}*/
			con.SendCommand(uid + "wait_do");					// prepare to operation
			con.SendCommand(uid + "start");						// start new command
			con.SendCommand(uid + "true");						// query
			con.SendCommand(uid + scalar.ToString().ToLower());	// returns data if scalar = false
			con.SendCommand(uid + query);						// query string
			if (parameters != null)
				foreach (var x in parameters)
			{
				con.SendCommand(uid + "wait_key");
				con.SendCommand(uid + x.Key);
				con.SendCommand(uid + x.Value);
			}
			con.SendCommand(uid + "wait_do");
			con.SendCommand(uid + "operation");
			if (scalar) return;
			int headerLength = Convert.ToInt32(con.SendCommand(uid + "next"));
			string[] header = new string[headerLength];
			for (int i = 0; i < headerLength; ++i)
			{
				header[i] = con.SendCommand(uid + "next");
			}
			int rowCount = Convert.ToInt32(con.SendCommand(uid + "next"));
			string[][] table = new string[rowCount][];
			for (int i = 0; i < rowCount; ++i)
			{
				table[i] = new string[headerLength];
				for (int j = 0; j < headerLength; ++j)
				{
					table[i][j] = con.SendCommand(uid + "next");
				}
			}
			ExposeTableToGridView(header, table, ChosenMainTable);
		}
		/// <summary>
		/// Changes the chosen table.
		/// </summary>
		private void ChangeChosenTable()
		{
			currentId = 0;
			SecondaryTable.Rows.Clear();
			SecondaryTable.Columns.Clear();
			string newChoice = GetMainTableChoice();
			Dictionary < string, string > procedures = new Dictionary<string, string>();
			procedures.Add("Items", "getItems");
			procedures.Add("Storages", "getStorages");
			procedures.Add("Presences", "getAllPresences");
			if (!procedures.ContainsKey(newChoice))
			{
				PrimaryTableLabel.Text = "";
				if (newChoice == "query")
				{
					query q = new query();
					q.SetMainForm(this);
					q.ShowDialog();
					q.OnClose(null, null);
					string fq = q.GetFormedQuery();
					Dictionary < string, object > fp = q.GetFormedParameters();
					if (q.Formed)
						RunQuery(fq, fp);
					return;
				}
				else
				{
					MessageBox.Show("Error: Wrong chosen table");
					return;
				}
			}
			Dictionary < string, string > labels = new Dictionary<string, string>();
			labels.Add ("Items", "Choose item to show its availability at storages");
			labels.Add ("Storages", "Choose storage to show which items are available at it");
			labels.Add ("Presences", "Choose presence(itemid - to show this item, storageid - to show this storage)");
			PrimaryTableLabel.Text = labels[newChoice];
			string command = procedures[newChoice];
			//MySqlDataReader dr = GetDataFromStoredProcedure(command, null);
			//ExposeReaderToTable(dr, ChosenMainTable);
			GetDataFromStoredProcedure(command, null, ChosenMainTable);
//			bool firstPass = true;
//			ChosenMainTable.Rows.Clear();
//			ChosenMainTable.Columns.Clear();
//			while (dr.Read ())
//			{
//				if (firstPass)
//				{
//					for (int i = 0; i < dr.FieldCount; ++i)
//						ChosenMainTable.Columns.Add(dr.GetName(i), dr.GetName(i));
//					firstPass = false;
//				}
//				//string[] row = new string[dr.FieldCount];
//				int n = 0;
//				try
//				{
//					n = ChosenMainTable.Rows.Add();
//				}
//				catch (Exception e)
//				{
//					MessageBox.Show(e.Message);
//				}
//				for (int i = 0; i < dr.FieldCount; ++i)
//					//row[i] = dr.GetString(i);
//					ChosenMainTable.Rows[n].Cells[i].Value = dr.GetString(i);
//				//ChosenMainTable.Rows.Add(row);
//			}
//			//ChosenMainTable.DataSource = dr;
//			dr.Close();
		}
		/// <summary>
		/// Initializes the connection.
		/// </summary>
		private void InitializeConnection()
		{
			string constr = 
				"Server=localhost;" +
				"Database=market;" +
				"User ID=root;" +
				"Password=123Vjq123;" +
				"Pooling=false";
			dbcon = new MySqlConnection(constr);
			dbcon.Open ();
		}
		/// <summary>
		/// Gets the sql command.
		/// </summary>
		public MySqlCommand GetSqlCommand()
		{
			return dbcon.CreateCommand();
		}
		/// <summary>
		/// Exposes the table to grid view.
		/// </summary>
		/// <param name='header'>
		/// Header.
		/// </param>
		/// <param name='table'>
		/// Table.
		/// </param>
		/// <param name='gv'>
		/// Gv.
		/// </param>
		private void ExposeTableToGridView(string[] header, string[][] table, DataGridView gv)
		{
			deletingEnabled = false;
			if (gv.Rows.Count > 0 && gv.Rows[0].Cells.Count > 0)
				gv.CurrentCell = gv.Rows[0].Cells[0];
			gv.Columns.Clear();
			int headerLength = header.Length;
			int rowCount = table.Length;
			for (int i = 0; i < headerLength; ++i)
				gv.Columns.Add(header[i], header[i]);
			maxId = 0;
			int n = 0;
			for (int i = 0; i < rowCount; ++i)
			{
				if (gv == ChosenMainTable)
				{
					int id = Convert.ToInt32(table[i][0]);
					if (id > maxId)
						maxId = id;
				}
				n = gv.Rows.Add ();
				for (int j = 0; j < headerLength; ++j)
				{
					gv.Rows[n].Cells[j].Value = table[i][j];
				}
			}
			deletingEnabled = true;
		}
		/// <summary>
		/// Gets the data from stored procedure.
		/// </summary>
		/// <returns>
		/// The data from stored procedure.
		/// </returns>
		/// <param name='storedProcedureName'>
		/// Stored procedure name.
		/// </param>
		/// <param name='parameters'>
		/// Parameters.
		/// </param>
		private void GetDataFromStoredProcedure(
			string storedProcedureName, 
			Dictionary < string, object > parameters,
			DataGridView gv)
		{
			/*MySqlCommand cmd = GetSqlCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = storedProcedureName;
			if (parameters != null)
				foreach(var x in parameters)
			{
				cmd.Parameters.Add(x.Key, x.Value);
			}
			return cmd.ExecuteReader();*/
			con.SendCommand(uid + "wait_do");				// prepare to operation
			con.SendCommand(uid + "start");				// start new command
			con.SendCommand(uid + "false");				// stored procedure
			con.SendCommand(uid + "false");				// returns data
			con.SendCommand(uid + storedProcedureName);	// procedure name
			if (parameters != null)
				foreach (var x in parameters)
			{
				con.SendCommand(uid + "wait_key");
				con.SendCommand(uid + x.Key);
				con.SendCommand(uid + x.Value);
			}
			con.SendCommand(uid + "wait_do");
			con.SendCommand(uid + "operation");
			int headerLength = Convert.ToInt32(con.SendCommand(uid + "next"));
			string[] header = new string[headerLength];
			for (int i = 0; i < headerLength; ++i)
			{
				header[i] = con.SendCommand(uid + "next");
			}
			int rowCount = Convert.ToInt32(con.SendCommand(uid + "next"));
			string[][] table = new string[rowCount][];
			for (int i = 0; i < rowCount; ++i)
			{
				table[i] = new string[headerLength];
				for (int j = 0; j < headerLength; ++j)
				{
					table[i][j] = con.SendCommand(uid + "next");
				}
			}
			ExposeTableToGridView(header, table, gv);
		}
		/// <summary>
		/// Closes the connection.
		/// </summary>
		private void CloseConnection()
		{
			dbcon.Close();
			dbcon = null;
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="lab7.mainform"/> class.
		/// </summary>
		public mainform ()
		{
			con.NewConnection();
			uid = con.SendCommand("0 begin") + " ";
			InitializeComponent();
			InitializeConnection();
		}
		/// <summary>
		/// Releases unmanaged resources and performs other cleanup operations before the <see cref="lab7.mainform"/> is
		/// reclaimed by garbage collection.
		/// </summary>
		~mainform()
		{
			con.SendCommand(uid + "exit");
			CloseConnection();
		}
		/// <summary>
		/// Gets the table.
		/// </summary>
		/// <returns>
		/// The table.
		/// </returns>
		private string GetTable()
		{
			if (GetMainTableChoice() == "Items") return "items";
			if (GetMainTableChoice() == "Storages") return "storages";
			if (GetMainTableChoice() == "Presences") return "presences";
			return "null";
		}
	}
}

