/*
 * Author: Fedorov Alexey
 * KKT
 * DonNU
 * 2012
 * Lab8 Var 2
 * Task:
 * 		- create database with three tables
 * 		- create stored procedures for work with data
 * 		- create dialog application to work with them (show tables, show special queries
 * edit tables, delete/add rows, create special query-builder (filters))
 * 		- thematics is market (items, storages and presences of items at storages)
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace lab7
{
	/// <summary>
	/// Query.
	/// </summary>
	public class query : Form
	{
		public bool Formed { get; set; }
		/// <summary>
		/// The formed query.
		/// </summary>
		private string formedQuery = "";
		/// <summary>
		/// The formed parameters.
		/// </summary>
		private Dictionary < string, object > formedParameters = new Dictionary<string, object>();
		/// <summary>
		/// The fields.
		/// </summary>
		private string[][] fields = new string[3][];
		/// <summary>
		/// The field counts.
		/// </summary>
		private int[] fieldCounts = new int[3];
		/// <summary>
		/// The field types.
		/// </summary>
		private Type[][] fieldTypes = new Type[3][];
		/// <summary>
		/// My main form.
		/// </summary>
		private mainform myMainForm;
		
		/// <summary>
		/// The constructor collection.
		/// </summary>
		private whereObj ConstructorCollection = new whereObj();
		
		/// <summary>
		/// The field chooser.
		/// </summary>
		private ComboBox FieldChooser = new ComboBox();
		/// <summary>
		/// The operation chooser.
		/// </summary>
		private ComboBox OperationChooser = new ComboBox();
		/// <summary>
		/// The value chooser.
		/// </summary>
		private ComboBox ValueChooser = new ComboBox();
		/// <summary>
		/// The commiter.
		/// </summary>
		private Button Commiter = new Button();
		/// <summary>
		/// The or commiter.
		/// </summary>
		private Button OrCommiter = new Button();
		/// <summary>
		/// The query box.
		/// </summary>
		private ListBox QueryBox = new ListBox();
		
		/// <summary>
		/// Sets the main form.
		/// </summary>
		/// <param name='frm'>
		/// Frm.
		/// </param>
		public void SetMainForm(mainform frm)
		{
			Formed = false;
			myMainForm = frm;
			GetAllSchemes();
		}
		/// <summary>
		/// Gets all schemes.
		/// </summary>
		private void GetAllSchemes()
		{
			MySqlCommand cmd = myMainForm.GetSqlCommand();
			cmd.CommandType = CommandType.Text;
			// get items scheme
			cmd.CommandText = "select * from items";
			MySqlDataReader dr = cmd.ExecuteReader();
			dr.Read ();
			fields[0] = new string[dr.FieldCount];
			fieldTypes[0] = new Type[dr.FieldCount];
			fieldCounts[0] = dr.FieldCount;
			for (int i = 0; i < dr.FieldCount; ++i)
			{
				fields[0][i] = string.Format("items.{0}", dr.GetName(i));
				FieldChooser.Items.Add(fields[0][i]);
				ValueChooser.Items.Add(fields[0][i]);
				fieldTypes[0][i] = dr.GetFieldType(i);
			}
			dr.Close();
			// get storages scheme
			cmd.CommandText = "select * from storages";
			dr = cmd.ExecuteReader();
			dr.Read ();
			fields[1] = new string[dr.FieldCount];
			fieldTypes[1] = new Type[dr.FieldCount];
			fieldCounts[1] = dr.FieldCount;
			for (int i = 0; i < dr.FieldCount; ++i)
			{
				fields[1][i] = string.Format("storages.{0}", dr.GetName(i));
				FieldChooser.Items.Add(fields[1][i]);
				ValueChooser.Items.Add(fields[1][i]);
				fieldTypes[1][i] = dr.GetFieldType(i);
			}
			dr.Close();
			// get presences scheme
			cmd.CommandText = "select * from presences";
			dr = cmd.ExecuteReader();
			dr.Read ();
			fields[2] = new string[dr.FieldCount];
			fieldTypes[2] = new Type[dr.FieldCount];
			fieldCounts[2] = dr.FieldCount;
			for (int i = 0; i < dr.FieldCount; ++i)
			{
				fields[2][i] = string.Format("presences.{0}", dr.GetName(i));
				FieldChooser.Items.Add(fields[2][i]);
				ValueChooser.Items.Add(fields[2][i]);
				fieldTypes[2][i] = dr.GetFieldType(i);
			}
			dr.Close();
		}
		/// <summary>
		/// Gets the formed parameters.
		/// </summary>
		/// <returns>
		/// The formed parameters.
		/// </returns>
		public Dictionary < string, object > GetFormedParameters()
		{
			return formedParameters;
		}
		/// <summary>
		/// Gets the formed query.
		/// </summary>
		/// <returns>
		/// The formed query.
		/// </returns>
		public string GetFormedQuery()
		{
			return formedQuery;
		}
		/// <summary>
		/// Initializes the form.
		/// </summary>
		private void InitializeForm()
		{
			ClientSize = new Size(600, 600);
			Text = "Special query";
		}
		/// <summary>
		/// Initializes the component.
		/// </summary>
		private void InitializeComponent()
		{
			InitializeForm();
			FieldChooser.Location = new Point(50, 50);
			FieldChooser.Size = new Size(100, 30);
			OperationChooser.Location = new Point(150, 50);
			OperationChooser.Size = new Size(40, 30);
			OperationChooser.Items.Add("<");
			OperationChooser.Items.Add("=");
			OperationChooser.Items.Add(">");
			OperationChooser.Items.Add("<>");
			OperationChooser.Items.Add("<=");
			OperationChooser.Items.Add(">=");
			ValueChooser.Location = new Point(190, 50);
			ValueChooser.Size = new Size(100, 30);
			Commiter.Location = new Point(290, 50);
			Commiter.Size = new Size(60, 20);
			Commiter.Text = "Add and";
			Commiter.Click += OnCommit;
			OrCommiter.Location = new Point(350, 50);
			OrCommiter.Size = new Size(60, 20);
			OrCommiter.Text = "Add or";
			OrCommiter.Click += OnOrCommit;
			QueryBox.Location = new Point(50, 100);
			QueryBox.Size = new Size(400, 300);
			Controls.Add(FieldChooser);
			Controls.Add(OperationChooser);
			Controls.Add(ValueChooser);
			Controls.Add(Commiter);
			Controls.Add(OrCommiter);
			Controls.Add(QueryBox);
			//this.Closing += OnClose;
		}
		/// <summary>
		/// Raises the commit event.
		/// </summary>
		/// <param name='sender'>
		/// Sender.
		/// </param>
		/// <param name='e'>
		/// E.
		/// </param>
		public void OnCommit(object sender, EventArgs e)
		{
			Formed = true;
			string field = "null";
			if (FieldChooser.SelectedItem != null)
				field = FieldChooser.SelectedItem.ToString();
			else return;
			string operation = "null";
			if (OperationChooser.SelectedItem != null)
				operation = OperationChooser.SelectedItem.ToString();
			else return;
			string val = "null";
			string valtype = "parameter";
			if (ValueChooser.SelectedItem != null)
			{
				val = ValueChooser.SelectedItem.ToString();
				valtype = "field";
			}
			else
			{
				val = ValueChooser.Text;
				int index = 0;
				if (field.Contains("storages.")) index = 1;
				if (field.Contains("presences.")) index = 2;
				int index2 = 0;
				for (; index2 < fieldCounts[index]; ++index2)
				{
					if (fields[index][index2] == field)
					{
						break;
					}
				}
				//if (fieldTypes[index][index2] != typeof(int))
				//{
				//	val = string.Format("\"{0}\"", val);
				//}
			}
			ConstructorCollection.Field = field;
			ConstructorCollection.Operation = operation;
			ConstructorCollection.SecondOperand = val;
			ConstructorCollection.SecondOperandType = valtype;
			QueryBox.Items.Add(ConstructorCollection.GetNextWhere());
		}
		/// <summary>
		/// Raises the or commit event.
		/// </summary>
		/// <param name='sender'>
		/// Sender.
		/// </param>
		/// <param name='e'>
		/// E.
		/// </param>
		public void OnOrCommit(object sender, EventArgs e)
		{
			Formed = true;
			string field = "null";
			if (FieldChooser.SelectedItem != null)
				field = FieldChooser.SelectedItem.ToString();
			else return;
			string operation = "null";
			if (OperationChooser.SelectedItem != null)
				operation = OperationChooser.SelectedItem.ToString();
			else return;
			string val = "null";
			string valtype = "parameter";
			if (ValueChooser.SelectedItem != null)
			{
				val = ValueChooser.SelectedItem.ToString();
				valtype = "field";
			}
			else
			{
				val = ValueChooser.Text;
				int index = 0;
				if (field.Contains("storages.")) index = 1;
				if (field.Contains("presences.")) index = 2;
				int index2 = 0;
				for (; index2 < fieldCounts[index]; ++index2)
				{
					if (fields[index][index2] == field)
					{
						break;
					}
				}
				//if (fieldTypes[index][index2] != typeof(int))
				//{
				//	val = string.Format("\"{0}\"", val);
				//}
			}
			ConstructorCollection.Field = field;
			ConstructorCollection.Operation = operation;
			ConstructorCollection.SecondOperand = val;
			ConstructorCollection.SecondOperandType = valtype;
			QueryBox.Items.Add(ConstructorCollection.GetNextWhereOr());
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="lab7.query"/> class.
		/// </summary>
		public query ()
		{
			InitializeComponent();
			//GetAllSchemes();
		}
		private string GetTablesList()
		{
			string res = "";
			string coma = "";
			string formedWhere = ConstructorCollection.GetFormedWhere();
			if (formedWhere.Contains("items."))
			{
				res = string.Format("{0}{1}items", res, coma);
				coma = ", ";
			}
			if (formedWhere.Contains("storages."))
			{
				res = string.Format("{0}{1}storages", res, coma);
				coma = ", ";
			}
			if (formedWhere.Contains("presences."))
			{
				res = string.Format("{0}{1}presences", res, coma);
				coma = ", ";
			}
			return res;
		}
		/// <summary>
		/// Raises the close event.
		/// </summary>
		/// <param name='sender'>
		/// Sender.
		/// </param>
		/// <param name='e'>
		/// E.
		/// </param>
		public void OnClose(object sender, EventArgs e)
		{
			formedQuery = string.Format("select * from {1} {0}", ConstructorCollection.GetFormedWhere(), GetTablesList());
			formedParameters = ConstructorCollection.GetFormedParams();
			//Formed = true;
		}
	}
}

