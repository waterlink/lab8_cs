/*
 * Author: Fedorov Alexey
 * KKT
 * DonNU
 * 2012
 * Lab8 Var 2
 * Task:
 * 		- create database with three tables
 * 		- create stored procedures for work with data
 * 		- create dialog application to work with them (show tables, show special queries
 * edit tables, delete/add rows, create special query-builder (filters))
 * 		- thematics is market (items, storages and presences of items at storages)
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace lab7
{
	/// <summary>
	/// Where object.
	/// </summary>
	public class whereObj
	{
		/// <summary>
		/// Gets or sets the field.
		/// </summary>
		/// <value>
		/// The field.
		/// </value>
		public string Field { get; set; }
		/// <summary>
		/// Gets or sets the operation.
		/// </summary>
		/// <value>
		/// The operation.
		/// </value>
		public string Operation { get; set; }
		/// <summary>
		/// Gets or sets the type of the second operand.
		/// </summary>
		/// <value>
		/// The type of the second operand.
		/// </value>
		public string SecondOperandType { get; set; }
		/// <summary>
		/// Gets or sets the second operand.
		/// </summary>
		/// <value>
		/// The second operand.
		/// </value>
		public string SecondOperand { get; set; }
		/// <summary>
		/// The started forming.
		/// </summary>
		private bool StartedForming = false;
		/// <summary>
		/// The formed where.
		/// </summary>
		private string FormedWhere = "";
		private Dictionary < string, object > FormedParams = new Dictionary<string, object>();
		/// <summary>
		/// The identifier.
		/// </summary>
		private int id = 0;
		/// <summary>
		/// Initializes a new instance of the <see cref="lab7.whereObj"/> class.
		/// </summary>
		public whereObj ()
		{
			Field = "";
			Operation = "";
			SecondOperand = "";
			SecondOperandType = "";
		}
		/// <summary>
		/// Gets the next where.
		/// </summary>
		/// <returns>
		/// The next where.
		/// </returns>
		public string GetNextWhere()
		{
			string res = "";
			if (!StartedForming)
			{
				if (SecondOperandType == "parameter")
				{
					res = string.Format("{0} {1} where_{2}", Field, Operation, id);
					FormedWhere = string.Format("where {0} {1} @where_{2}", Field, Operation, id);
					FormedParams.Add(string.Format ("@where_{0}", id), SecondOperand);
					++id;
				}
				else
				{
					res = string.Format("{0} {1} {2}", Field, Operation, SecondOperand);
					FormedWhere = string.Format("where {0} {1} {2}", Field, Operation, SecondOperand);
				}
				StartedForming = true;
			}
			else
			{
				if (SecondOperandType == "parameter")
				{
					res = string.Format("{0} {1} where_{2}", Field, Operation, id);
					FormedWhere = string.Format("{3} and {0} {1} @where_{2}", Field, Operation, id, FormedWhere);
					FormedParams.Add(string.Format ("@where_{0}", id), SecondOperand);
					++id;
				}
				else
				{
					res = string.Format("{0} {1} {2}", Field, Operation, SecondOperand);
					FormedWhere = string.Format("{3} and {0} {1} {2}", Field, Operation, SecondOperand, FormedWhere);
				}
			}
			return res;
		}
		/// <summary>
		/// Gets the next where or.
		/// </summary>
		/// <returns>
		/// The next where or.
		/// </returns>
		public string GetNextWhereOr()
		{
			string res = "";
			if (!StartedForming)
			{
				if (SecondOperandType == "parameter")
				{
					res = string.Format("{0} {1} where_{2}", Field, Operation, id);
					FormedWhere = string.Format("where {0} {1} @where_{2}", Field, Operation, id);
					FormedParams.Add(string.Format ("@where_{0}", id), SecondOperand);
					++id;
				}
				else
				{
					res = string.Format("{0} {1} {2}", Field, Operation, SecondOperand);
					FormedWhere = string.Format("where {0} {1} {2}", Field, Operation, SecondOperand);
				}
				StartedForming = true;
			}
			else
			{
				if (SecondOperandType == "parameter")
				{
					res = string.Format("{0} {1} where_{2}", Field, Operation, id);
					FormedWhere = string.Format("{3} or {0} {1} @where_{2}", Field, Operation, id, FormedWhere);
					FormedParams.Add(string.Format ("@where_{0}", id), SecondOperand);
					++id;
				}
				else
				{
					res = string.Format("{0} {1} {2}", Field, Operation, SecondOperand);
					FormedWhere = string.Format("{3} or {0} {1} {2}", Field, Operation, SecondOperand, FormedWhere);
				}
			}
			return res;
		}
		/// <summary>
		/// Gets the formed where.
		/// </summary>
		/// <returns>
		/// The formed where.
		/// </returns>
		public string GetFormedWhere()
		{
			return FormedWhere;
		}
		/// <summary>
		/// Gets the formed parameters.
		/// </summary>
		/// <returns>
		/// The formed parameters.
		/// </returns>
		public Dictionary < string, object > GetFormedParams()
		{
			return FormedParams;
		}
	}
}

